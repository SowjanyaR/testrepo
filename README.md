# AgGridCli

This project was generated with [Angular CLI].

# Clone the repo
Run `git clone https://SowjanyaR@bitbucket.org/SowjanyaR/testrepo.git`

Run `cd testrepo`

## Install npm
Run `npm install`

## Local server
Run `npm start` for a  local server. Navigate to `http://localhost:4200/`.

## Build
Run `ng build` to build the project.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma].
