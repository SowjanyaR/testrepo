export interface Person {
  name: string;
  height: number;
  weight: number;
  gender: string;
}
