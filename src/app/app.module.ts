import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import { HttpModule } from '@angular/http';
import {AgGridModule} from "ag-grid-angular/main";
import {AppComponent} from "./app.component";
import {MyGridApplicationComponent} from "./my-grid-application/my-grid-application.component";
import { PeopleService } from './people.service';

@NgModule({
    declarations: [
        AppComponent,
        MyGridApplicationComponent
    ],
    imports: [
      BrowserModule,
      HttpModule,
      AgGridModule.withComponents(
            []
        )
    ],
    providers: [PeopleService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
