import { Component, OnInit } from '@angular/core';

import {ColDef, ColumnApi ,GridApi ,GridOptions} from "ag-grid/main";

import { Person } from '../person';
import { PeopleService } from '../people.service';

@Component({
    selector: 'app-my-grid-application',
    templateUrl: './my-grid-application.component.html'
})
export class MyGridApplicationComponent implements OnInit{
    columnDefs: ColDef[]
    rowData: Person[];
    api: GridApi;
    columnApi: ColumnApi;
    errorMessage: string = '';
    gridOptions: GridOptions;


    constructor(private peopleService: PeopleService) {
      this.columnDefs= this.createColumnDefs();
      this.gridOptions = {
        // turn on filtering
        enableFilter: true,
        columnDefs: [{headerName: "Name", field: "name", filter: "agSetColumnFilter"}]
        };
    }
    ngOnInit() {
      this.peopleService
        .getAll()
        .subscribe(
          /* happy path */ p => this.rowData = p,
          /* error path */ e => this.errorMessage = e);
    }

    onGridReady(params) {
       this.api = params.api;
       this.columnApi = params.columnApi;
       this.api.sizeColumnsToFit();
    }
  createColumnDefs() {
      return[
        { field: 'name',  valueGetter: (params) => params.data.name},
        { field: 'height',  valueGetter: (params) => params.data.height},
        { field: 'weight',  valueGetter: (params) => params.data.weight},
        { field: 'gender',  valueGetter: (params) => params.data.gender}
       ];
  }
}

